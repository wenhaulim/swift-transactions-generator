# SWIFT Transactions Generator
A Python package that generates different MT type transactions with your desired field into a csv that can be imported in SQL.

# What is SWIFT?
SWIFT is a messaging network that financial institutions use to securely transmit information and instructions through a standardized system of codes. A SWIFT Message consists of 1 to 5 blocks that are:
1. Basic Header
2. Application Header
3. User Header
4. Text
5. Trailer

# Links
A few links to help you understand different type of SWIFT messages.
1. Anatomy of a SWIFT message. [link here](https://www.iotafinance.com/en/SWIFT-ISO15022-Message-type-MT101.html#purpose)
2. Structure. [link here](https://www.iotafinance.com/en/SWIFT-ISO15022-Message-type-MT101.html#purpose)
3. SWIFT message structure blocks. [link here](https://www.paiementor.com/swift-mt-message-structure-blocks-1-to-5/)

# Functionality
Currently the code has 2 MT Type Transaction classes (MT101 and MT202). <br>
They have similar methods like `generate_basci_header`, `generate_application_header`, `generate_user_header`, `generate_user_trailer`, `generate_SWIFT_MTXXX`. The only difference are the methods `update_text_fields_to_show`, and `generate_text`. <br> 
You can also 

# How to use
You can initialise a transaction type to a variable like so: <br>
```transaction = MT_101_transaction()``` <br>
It also accepts 3 fields, but it is already defaulted so that it still runs if you don't specify it.
- `start_datetime`: earliest date range for the random date generator
- `end_datetime`: latest date range for the random date generator
- `optional_fields`: list of optional fields that you want to show. Mandatory fields are already defaulted to true and do not need to specify. <br>

Once that variable has been initialised. You can do a: <br>
```t.generate_SWIFT_MTXXX()``` 
<br>
to generate a random transaction.

The function `generate_mixed_transactions` can also be used to generate a set of multiple different transactions into a list.

# TODO
1. Make a function for the writing of transactions into a csv
2. Code rest of the `Text` block rules for the different type of MT types

# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 21:04:21 2021

@author: wlim
"""

#%% Imports

from faker import Faker
from forex_python.converter import CurrencyRates
import random
import time
import string
import math
import csv

#%% Global Functions

def str_time_prop(start, end, time_format, prop):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formatted in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    stime = time.mktime(time.strptime(start, time_format))
    etime = time.mktime(time.strptime(end, time_format))

    ptime = stime + prop * (etime - stime)

    return time.strftime(time_format, time.localtime(ptime))

def random_date(start, end, prop):
    return str_time_prop(start, end, '%d/%m/%Y %H:%M', prop)

def create_random_datetime_between(start_datetime="1/1/2018 00:00", end_datetime="31/12/2021 23:59"):
    """ generate random date used in MT Transactions classes"""
    random_datetime = random_date(start_datetime, end_datetime, random.random())
    return random_datetime

#%% Initialise locale and available currencies

currencies = list(CurrencyRates().get_rates("USD").keys())
faker = Faker("en_GB")

#%% MT101 Class

class MT_101_transaction:
    """
    accepts 3 arguments:
        start_datetime: earliest date range for the random date generator
        end_datetime: latest date range for the random date generator
        optional_fields: list of optional fields that you want to show. Mandatory fields do not need to specify.
    """
    
    # CONSTRUCTOR: pre-defined variables (range of transactions' datetime)
    def __init__(self, start_datetime:str="1/1/2008 1:30", end_datetime:str="1/1/2009 23:50",optional_fields:list=[]):
        self.start_datetime = start_datetime
        self.end_datetime = end_datetime
        self.optional_fields = optional_fields

    # METHOD: Update fields to show
    def update_text_fields_to_show(self):
        default_option_dict = {
                "20": True,
                "21": True,
                "28D": True,
                "30": True,
                "32B": True,
                "59a": True,
                "71A": True,
                "21F": False,
                "21R": False,
                "23E": False,
                "25": False,
                "25A": False,
                "33B": False,
                "36": False,
                "50a": False,
                "51A": False,
                "52a": False,
                "56a": False,
                "57a": False,
                "70": False,
                "77B": False
            }
        # update text fields that want to be shown
        for column in self.optional_fields:
            if column not in default_option_dict.keys():
                raise ValueError("Wrong column name/ not a string")
            default_option_dict[column] = True        
        
        columns_wanted = []
        for key, value in default_option_dict.items():
            if value == True:
                columns_wanted.append(key)
        return columns_wanted
        
        
    # METHOD: Basic headers
    def generate_basic_header(self):
        start_01 = "{1:"
        application_id = "F"
        service_id = "01"
        terminal_address_source = (
            "".join(random.choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ") for i in range(9))
            + "XXX"
        )
        session_number = "{:04d}".format(random.randrange(1, 9999))
        sequence_number = "{:02d}".format(random.randrange(1, 99))
        end_01 = "}"

        basic_header = (
            start_01
            + application_id
            + service_id
            + terminal_address_source
            + session_number
            + sequence_number
            + end_01
        )
        return basic_header
    
    # METHOD: Application Header
    def generate_application_header(self,random_datetime):
        start_02 = "{2:"
        direction = random.choice(["I", "O"])
        message_type = "101"
        input_time = random_datetime[11:13] + random_datetime[14:16]
        input_date = random_datetime[8:10] + random_datetime[3:5] + random_datetime[0:2]
        terminal_address_destination = (
            "".join(random.choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ") for i in range(9)) + "XXX"
        )
        session_number = "{:04d}".format(random.randrange(1, 9999))
        sequence_number = "{:02d}".format(random.randrange(1, 99))
        output_date = input_date
        output_time = input_time
        priority = random.choice(["S", "U", "N"])
        delivery_monitoring = str(random.randrange(1, 4))
        obsolescence_period = "{:03d}".format(random.randrange(1, 999))
        end_02 = "}"

        if direction == "I":
            #     without time and date (input nor output)
            application_header = (
                start_02
                + direction
                + message_type
                + terminal_address_destination
                + priority
                + delivery_monitoring
                + obsolescence_period
                + end_02
            )
        else:
            #     without delivery monitoring and obsolesence period
            application_header = (
                start_02
                + direction
                + message_type
                + input_time
                + input_date
                + terminal_address_destination
                + session_number
                + sequence_number
                + output_date
                + output_time
                + priority
                + end_02
            )
        return application_header
    
    
    # METHOD: User Header
    def generate_user_header(self):
        start_03 = "{3:"
        open_bracket = "{"
        tag_103 = "103"
        colon = ":"
        service_identifier = "".join(
            random.choice(string.ascii_uppercase) for i in range(3)
        )
        close_bracket = "}"
        open_bracket = "{"
        tag_108 = "108"
        colon = ":"
        message_user_reference = "".join(
            random.choice(string.ascii_uppercase + string.digits) for i in range(16)
        )
        close_bracket = "}"
        end_03 = "}"

        user_header = (
            start_03
            + open_bracket
            + tag_103
            + colon
            + service_identifier
            + close_bracket
            + open_bracket
            + tag_108
            + colon
            + message_user_reference
            + close_bracket
            + end_03
        )
        return user_header
    
    # METHOD: Text
    def generate_text(self,random_datetime):
        # initialise the columns that we want printed out
        columns_wanted = self.update_text_fields_to_show()
        # start string concatenation algorithm
        string_to_concat = ["{4:\n"]
        for column in columns_wanted:
            if column == "20": 
                string_to_concat.append(":20:"+ "".join(random.choice(string.ascii_uppercase + string.digits) for i in range(16))+ "\n")
            elif column == "21":
                string_to_concat.append(":21:"+ "".join(random.choice(string.ascii_uppercase + string.digits) for i in range(16))+ "\n")
            elif column == "28D":
                string_to_concat.append(":28D:" + "1/1" + "\n")
            elif column == "30":
                input_date = random_datetime[8:10] + random_datetime[3:5] + random_datetime[0:2]
                string_to_concat.append(":30:" + input_date + "\n")
            elif column == "32B":
                currency = random.choice(currencies)
                string_to_concat.append(":32B:" + currency + str(random.randrange(1, 9999999) / 100) + "\n")
            elif column == "59a":
                string_to_concat.append(":59A:" + faker.name() + "\n" + faker.address() + "\n")
            elif column == "71A":
                string_to_concat.append(":71A:"+ "".join(random.choice(string.ascii_uppercase) for i in range(3))+ "\n")
            elif column == "21F":
                string_to_concat.append(":21F:"+ "........"+ "\n")
            elif column == "21R":
                string_to_concat.append(":21R:"+ "........"+ "\n")
            elif column == "23E":
                string_to_concat.append(":23E:"+ "........"+ "\n")
            elif column == "25": 
                string_to_concat.append(":25:"+ "........"+ "\n")
            elif column == "25A":
                string_to_concat.append(":25A:"+ "........"+ "\n")
            elif column == "33B":
                string_to_concat.append(":33B:"+ "........"+ "\n")
            elif column == "36": 
                string_to_concat.append(":36:"+ "........"+ "\n")
            elif column == "50a":
                string_to_concat.append(":50a:"+ "........"+ "\n")
            elif column == "51A":
                string_to_concat.append(":51A:"+ "........"+ "\n")
            elif column == "52a":
                string_to_concat.append(":52a:"+ "........"+ "\n")
            elif column == "56a":
                string_to_concat.append(":56a:"+ "........"+ "\n")
            elif column == "57a":
                string_to_concat.append(":57a:"+ "........"+ "\n")
            elif column == "70": 
                string_to_concat.append(":70:"+ "........"+ "\n")
            elif column == "77B":
                string_to_concat.append(":77B:"+ "........"+ "\n")
        string_to_concat.append("-}")
        text = "".join(string_to_concat)
        return text
    
    # METHOD: User Trailer
    def generate_user_trailer(self):
        start_05 = "{5:"
        open_bracket = "{"
        checksum = "CHK"
        colon = ":"
        content = "".join(
            random.choice(string.ascii_uppercase + string.digits) for i in range(12)
        )
        close_bracket = "}"
        end_05 = "}"

        user_trailer = (
            start_05 + open_bracket + checksum + colon + content + close_bracket + end_05
        )
        return user_trailer
    
#    METHOD: Final Generate transactions
    def generate_SWIFT_MT101(self):
        random_datetime = create_random_datetime_between(self.start_datetime, self.end_datetime)
        MT101 = (
            self.generate_basic_header()
            + self.generate_application_header(random_datetime)
            + self.generate_user_header()
            + self.generate_text(random_datetime)
            + self.generate_user_trailer()
        )
        return MT101

#%% MT202 Class

class MT_202_transaction:
    """
    accepts 3 arguments:
        start_datetime: earliest date range for the random date generator
        end_datetime: latest date range for the random date generator
        optional_fields: list of optional fields that you want to show. Mandatory fields do not need to specify.
    """
    # CONSTRUCTOR: pre-defined variables (range of transactions' datetime)
    def __init__(self, start_datetime="1/1/2008 1:30", end_datetime="1/1/2009 23:50",optional_fields:list=[]):
        self.start_datetime = start_datetime
        self.end_datetime = end_datetime
        self.optional_fields = optional_fields

    # METHOD: Update fields to show
    def update_text_fields_to_show(self):
        default_option_dict = {
                "20": True,
                "21": True,
                "32A": True,
                "58a": True,
                "13C": False,
                "52a": False,
                "53a": False,
                "54a": False,
                "56a": False,
                "57a": False,
                "72": False
            }
        # update text fields that want to be shown
        for column in self.optional_fields:
            if column not in default_option_dict.keys():
                raise ValueError("Wrong column name/ not a string")
            default_option_dict[column] = True        
        
        columns_wanted = []
        for key, value in default_option_dict.items():
            if value == True:
                columns_wanted.append(key)
        return columns_wanted
        
    # METHOD: Basic headers
    def generate_basic_header(self):
        start_01 = "{1:"
        application_id = "F"
        service_id = "01"
        terminal_address_source = (
            "".join(random.choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ") for i in range(9))
            + "XXX"
        )
        session_number = "{:04d}".format(random.randrange(1, 9999))
        sequence_number = "{:02d}".format(random.randrange(1, 99))
        end_01 = "}"

        basic_header = (
            start_01
            + application_id
            + service_id
            + terminal_address_source
            + session_number
            + sequence_number
            + end_01
        )
        return basic_header
    
    # METHOD: Application Header
    def generate_application_header(self,random_datetime):
        start_02 = "{2:"
        direction = random.choice(["I", "O"])
        message_type = "202"
        input_time = random_datetime[11:13] + random_datetime[14:16]
        input_date = random_datetime[8:10] + random_datetime[3:5] + random_datetime[0:2]
        terminal_address_destination = (
            "".join(random.choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ") for i in range(9)) + "XXX"
        )
        session_number = "{:04d}".format(random.randrange(1, 9999))
        sequence_number = "{:02d}".format(random.randrange(1, 99))
        output_date = input_date
        output_time = input_time
        priority = random.choice(["S", "U", "N"])
        delivery_monitoring = str(random.randrange(1, 4))
        obsolescence_period = "{:03d}".format(random.randrange(1, 999))
        end_02 = "}"

        if direction == "I":
            #     without time and date (input nor output)
            application_header = (
                start_02
                + direction
                + message_type
                + terminal_address_destination
                + priority
                + delivery_monitoring
                + obsolescence_period
                + end_02
            )
        else:
            #     without delivery monitoring and obsolesence period
            application_header = (
                start_02
                + direction
                + message_type
                + input_time
                + input_date
                + terminal_address_destination
                + session_number
                + sequence_number
                + output_date
                + output_time
                + priority
                + end_02
            )
        return application_header
    
    
    # METHOD: User Header
    def generate_user_header(self):
        start_03 = "{3:"
        open_bracket = "{"
        tag_103 = "103"
        colon = ":"
        service_identifier = "".join(
            random.choice(string.ascii_uppercase) for i in range(3)
        )
        close_bracket = "}"
        open_bracket = "{"
        tag_108 = "108"
        colon = ":"
        message_user_reference = "".join(
            random.choice(string.ascii_uppercase + string.digits) for i in range(16)
        )
        close_bracket = "}"
        end_03 = "}"

        user_header = (
            start_03
            + open_bracket
            + tag_103
            + colon
            + service_identifier
            + close_bracket
            + open_bracket
            + tag_108
            + colon
            + message_user_reference
            + close_bracket
            + end_03
        )
        return user_header
    
    # METHOD: Text
    def generate_text(self, random_datetime):
        # initialise the columns that we want printed out
        columns_wanted = self.update_text_fields_to_show()
        # start string concatenation algorithm
        string_to_concat = ["{4:\n"]
        for column in columns_wanted:
            if column == "20": 
                string_to_concat.append(":20:"+ "".join(random.choice(string.ascii_uppercase + string.digits) for i in range(16))+ "\n")
            elif column == "21":
                string_to_concat.append(":21:"+ "".join(random.choice(string.ascii_uppercase + string.digits) for i in range(16))+ "\n")
            elif column == "32A":
                input_date = random_datetime[8:10] + random_datetime[3:5] + random_datetime[0:2]
                currency = random.choice(currencies)
                string_to_concat.append(":32A:" + input_date + currency + str(random.randrange(1, 9999999) / 100) + "\n")
            elif column == "58a":
                string_to_concat.append(":58A:" + faker.iban() + "\n" + faker.name() + "\n" + faker.address() + "\n")
            elif column == "13C":
                string_to_concat.append(":13C:"+ "........"+ "\n")
            elif column == "52a":
                string_to_concat.append(":52a:"+ "........"+ "\n")
            elif column == "53a":
                string_to_concat.append(":53a:"+ "........"+ "\n")
            elif column == "54a":
                string_to_concat.append(":54a:"+ "........"+ "\n")
            elif column == "56a":
                string_to_concat.append(":56a:"+ "........"+ "\n")
            elif column == "57a":
                string_to_concat.append(":57a:"+ "........"+ "\n")
            elif column == "72": 
                string_to_concat.append(":72:"+ "........"+ "\n")
        string_to_concat.append("-}")
        text = "".join(string_to_concat)
        return text
    
    # METHOD: User Trailer
    def generate_user_trailer(self):
        start_05 = "{5:"
        open_bracket = "{"
        checksum = "CHK"
        colon = ":"
        content = "".join(
            random.choice(string.ascii_uppercase + string.digits) for i in range(12)
        )
        close_bracket = "}"
        end_05 = "}"

        user_trailer = (
            start_05 + open_bracket + checksum + colon + content + close_bracket + end_05
        )
        return user_trailer
    
    # METHOD: Final Generate transactions
    def generate_SWIFT_MT202(self):
        random_datetime = create_random_datetime_between(self.start_datetime, self.end_datetime)
        MT202 = (
            self.generate_basic_header()
            + self.generate_application_header(random_datetime)
            + self.generate_user_header()
            + self.generate_text(random_datetime)
            + self.generate_user_trailer()
        )
        return MT202



#%% Produce transactions function

def generate_mixed_transactions(transaction_types:list, total_transactions:int):
    
    transactions = []
    counter = 1
    each_type_total = math.ceil(total_transactions/len(transaction_types))
    
    for transaction_type in transaction_types:
        if transaction_type == 'MT101':
            for _ in range(each_type_total):
                transactions.append([counter, 'MT101', MT_101_transaction().generate_SWIFT_MT101()])
                counter += 1
        elif transaction_type == 'MT202':
            for _ in range(each_type_total):
                transactions.append([counter, 'MT202', MT_202_transaction().generate_SWIFT_MT202()])
                counter += 1
        else:
            raise ValueError('Transaction MT type is not correct/ has not been built')
            
    return transactions


#%% Produce transactions 

transactions = generate_mixed_transactions(['MT101', 'MT202'], 1000)


#%% Export to CSV

with open("transactions.csv", mode="w", newline="") as csv_file:
    csv_writer = csv.writer(csv_file, delimiter="|")
    csv_writer.writerow(["ID", "MT_Type", "MT_Transaction"])
    for row in transactions:
        csv_writer.writerow(row)

csv_file.close()


#%% TODO
# add accepting other MT type function - DONE
# add number of transactions to csv - DONE
# adding mandatory and optional functions option - DONE

# change regex into hardcode
# code rest of the text fields for each transaction















